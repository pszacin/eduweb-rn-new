/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  Keyboard,
} from 'react-native';

import styled from 'styled-components/native';
import ProfileScreen from './ProfileScreen';

//

const App = () => {
  return <ProfileScreen />;
};

export default App;

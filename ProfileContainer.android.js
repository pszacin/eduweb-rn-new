import React, {useState} from 'react';
import {SafeAreaView, KeyboardAvoidingView, Platform} from 'react-native';

//

const ProfileContainer = props => {
  return <SafeAreaView>{props.children}</SafeAreaView>;
};

export default ProfileContainer;

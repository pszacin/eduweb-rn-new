import React, {useState} from 'react';
import {SafeAreaView, KeyboardAvoidingView, Platform} from 'react-native';

//

const ProfileContainer = props => {
  return (
    <KeyboardAvoidingView behavior={'padding'}>
      <SafeAreaView>{props.children}</SafeAreaView>
    </KeyboardAvoidingView>
  );
};

export default ProfileContainer;

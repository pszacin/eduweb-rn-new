import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import ProfileContainer from './ProfileContainer';
import styled from 'styled-components/native';

//

const profileImage = require('./assets/image.jpg');

const isAndroid = Platform.OS === 'android';

//

const ProfileScreen = () => {
  const [level, setLevel] = useState(1);
  const [name, setName] = useState('Jan Nowak');
  const [currentText, setCurrentText] = useState('');

  function onUpgradePress() {
    setLevel(level + 1);
  }

  function onSaveName() {
    if (currentText.length < 3) {
      return;
    }

    setCurrentText('');
    setName(currentText);

    Keyboard.dismiss();
  }

  return (
    <ProfileContainer>
      <Container>
        <MainContent>
          <ProfileImageContainer>
            <ProfileImage resizeMode={'cover'} source={profileImage} />
            <LevelIndicator>
              <LevelIndicatorText>{level}</LevelIndicatorText>
            </LevelIndicator>
          </ProfileImageContainer>
          <View>
            <NameText>{name}</NameText>
          </View>

          <UpgradeButton
            onPress={onUpgradePress}
            onPressIn={() => console.log('onPressIn')}
            onPressOut={() => console.log('onPressOut')}>
            <UpgradeButtonText>{'UPGRADE'}</UpgradeButtonText>
          </UpgradeButton>
        </MainContent>

        <ChangeNameContainer>
          <NameTextInput
            placeholder={'Write new name...'}
            value={currentText}
            onChangeText={setCurrentText}
            returnKeyType={'done'}
            onSubmitEditing={onSaveName}
          />
          <NameButton onPress={onSaveName}>
            <NameButtonText>{'SET'}</NameButtonText>
          </NameButton>
        </ChangeNameContainer>
      </Container>
    </ProfileContainer>
  );
};

//

const Container = styled(View)`
  background-color: #eee;
  width: 100%;
  height: 100%;
`;

const MainContent = styled(View)`
  width: 100%;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const ProfileImageContainer = styled(View)``;

const ProfileImage = styled(Image)`
  width: 100px;
  height: 100px;
  background-color: #ff0000;
  border-radius: 50px;
  margin-bottom: 20px;
`;

const NameText = styled(Text)`
  text-align: center;
  font-weight: 600;
  font-size: 25px;
`;

const UpgradeButton = styled(TouchableOpacity)`
  background-color: blue;
  padding: 8px;
  margin-top: 20px;
  border-radius: 8px;
`;

const UpgradeButtonText = styled(Text)`
  text-align: center;
  color: white;
  font-weight: bold;
  font-size: 15px;
`;

const LevelIndicator = styled(View)`
  width: 20px;
  height: 20px;
  background-color: red;
  border-radius: 10px;

  position: absolute;
  top: 0;
  right: 0;
`;

const LevelIndicatorText = styled(Text)`
  color: white;
  font-weight: bold;
  text-align: center;
`;

const NameTextInput = styled(TextInput)`
  height: 100%;
  background-color: #aaaaaa;
  padding: 8px;
  flex: 1;
`;

const NameButton = styled(TouchableOpacity)`
  width: 80px;
  height: 100%;
  background-color: blue;
  justify-content: center;
`;

const NameButtonText = styled(Text)`
  text-align: center;
  color: white;
  font-weight: bold;
  font-size: 15px;
`;

const ChangeNameContainer = styled(View)`
  display: flex;
  flex-direction: row;
  height: 50px;
  margin-top: 20px;
`;

export default ProfileScreen;
